#!/bin/sh
case $1 in
	period-changed)
		killall -s USR1 py3status
		exec notify-send "Redshift" "Period changed to $3"
esac
