# Readme #

MSI GS65 Stealth Thin 8RE

### Main installation ###

Follow installation guide. Some choices:

#### Networking ####

Install and enable network manager and drivers.

```
pacman -S networkmanager
systemctl enable NetworkManager
```

### General recommendations ###

Follow general recommendations. Some choices:

#### Display server ####

Install Xorg.

```
pacman -S xorg
```

#### Display drivers ####

Install nouveau so that we can tunnel iGPU output through eGPU external screens (Mini DisplayPort, HDMI).

```
pacman -S xf86-video-nouveau
```

#### Window managers ####

Install i3 and some basics from MATE desktop.

```
pacman -S i3 dmenu ttf-dejavu mate-terminal caja pluma eom atril engrampa unrar
```

#### Display manager ####

Prefer startx as described on wiki.

```
pacman -S xorg-xinit
cp -v /etc/X11/xinit/xinitrc ~/.xinitrc
cp -v /etc/X11/xinit/xserverrc ~/.xserverrc
```

Make sure the part of `.xinitrc` prepares X and starts i3:

```
numlockx
mainmon
redshift -t 6500:3700 -r &

exec i3
```

The parameters for `picom` composition manager set quicker fading + the rest are taken from here and fix tearing for eGPU-tunneled external monitors: https://wiki.archlinux.org/index.php/Nouveau#Vertical_Sync

Add `-ardelay 250` to startup parameters in `.xserverrc`.

Create a `.zprofile` as described on wiki to startx automatically.

```
if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
  exec startx
fi
```

#### Power management ####

!Not sure if this helps!

Install and enable tlp, see wiki.

```
pacman -S tlp
systemctl enable tlp
```

#### Chromium ####

```
pacman -S chromium pepper-flash gnome-keyring
```

#### PulseAudio ####

```
pacman -S pulseaudio-alsa pavucontrol
```

#### Console #####

```
pacman -S zsh git
```

Install oh-my-zsh from https://github.com/robbyrussell/oh-my-zsh

## Configs and scripts ##

Clone this repository. Put files in place. No files should overwrite any existing files.

Install packages required for configs and scripts:

```
pacman -S xss-lock redshift py3status feh scrot playerctl picom dunst numlockx brightnessctl
```

Some are in AUR:

```
xkblayout-state-git
```

## Further configuration ##

### Desktop background ###

```
feh --bg-fill ~/Pictures/back.jpg
```

### i3 config ###

#### Variables ####

Update `$refresh_i3status` setting to update `py3status`.

#### Font ####

Increate monospace font size to 9. It's more readable.

#### General ####

```
focus_follows_mouse no
new_window pixel
```

#### File manager ####

```
bindsym $mod+Shift+Return exec caja
```

#### Move workspace to monitor ####

```
bindsym $mod+Mod1+Shift+Left move workspace to output left
bindsym $mod+Mod1+Shift+Right move workspace to output right
bindsym $mod+Mod1+Shift+Up move workspace to output up
bindsym $mod+Mod1+Shift+Down move workspace to output down
```

#### Brightness ####

Add user to `video` group.

```
bindsym XF86MonBrightnessUp exec --no-startup-id brightnessctl -d acpi_video0 s +3% && $refresh_i3status
bindsym XF86MonBrightnessDown exec --no-startup-id brightnessctl -d acpi_video0 s 3%- && $refresh_i3status
bindsym Shift+F1 exec --no-startup-id brightnessctl -d acpi_video0 s 0% && $refresh_i3status
bindsym Shift+F2 exec --no-startup-id brightnessctl -d acpi_video0 s 50% && $refresh_i3status
bindsym Shift+F3 exec --no-startup-id brightnessctl -d acpi_video0 s 100% && $refresh_i3status
```

#### Sound ####

Update volume control bindsyms to change volume only by 5%.

```
bindsym XF86AudioPlay exec --no-startup-id playerctl play-pause
bindsym XF86AudioNext exec --no-startup-id playerctl next
bindsym XF86AudioPrev exec --no-startup-id playerctl previous
```

#### Screenshots ####

```
bindsym --release $mod+Shift+S exec --no-startup-id scrot -s
bindsym $mod+Shift+W exec --no-startup-id scrot -u
```

#### Lock ####

```
bindsym $mod+Mod1+l exec --no-startup-id i3lock
```

#### Keyboard ####

```
bindsym $mod+t exec --no-startup-id xkblayout-state set +1 && $refresh_i3status
```

#### Power ####

```
set $mode_system System (s)uspend, (r)eboot, (p)oweroff, (e)xit
mode "$mode_system" {
    bindsym s exec --no-startup-id systemctl suspend, mode "default"
    bindsym r exec --no-startup-id systemctl reboot, mode "default"
    bindsym p exec --no-startup-id systemctl poweroff -i, mode "default"
    bindsym e exec --no-startup-id i3-msg exit

    bindsym Escape mode "default"
}

bindsym $mod+Shift+e mode "$mode_system"
```

#### Monitors ####

```
set $mode_monitor External monitor (o)ff, (z)entity, (a)bove, (b)elow, (l)eft, (r)ight, (m)irror
mode "$mode_monitor" {
    bindsym o exec --no-startup-id mainmon, mode "default"
    bindsym z exec --no-startup-id zentity, mode "default"
    bindsym a exec --no-startup-id singlemon --above, mode "default"
    bindsym b exec --no-startup-id singlemon --below, mode "default"
    bindsym l exec --no-startup-id singlemon --left-of, mode "default"
    bindsym r exec --no-startup-id singlemon --right-of, mode "default"
    bindsym m exec --no-startup-id singlemonmirror, mode "default"

    bindsym Escape mode "default"
}

bindsym $mod+Shift+m mode "$mode_monitor"
```

#### Bar ####

Replace `status_command i3status` with `status_command py3status`. 

#### Colors ####

Replace `#06A284` with chosen main theme color.

Replace `#06d4ac` with a lighter version of the same color.

```
# class                 border  backgr. text    indicator child_border
client.focused          #06d4ac #06A284 #ffffff #06d4ac	  #06A284 
client.focused_inactive #333333 #5f676a #ffffff #484e50   #5f676a
client.unfocused        #333333 #222222 #888888 #292d2e   #222222
client.urgent           #2f343a #900000 #ffffff #900000   #900000
client.placeholder      #000000 #0c0c0c #ffffff #000000   #0c0c0c

client.background       #ffffff
```

Add this inside bar configuration, also replacing the colors as above.

```
colors {
         	background #000000
 	        statusline #ffffff
 	        separator #666666
 
        	focused_workspace  #06d4ac #06A284 #ffffff 
	        active_workspace   #333333 #5f676a #ffffff
	        inactive_workspace #333333 #222222 #888888
	        urgent_workspace   #2f343a #900000 #ffffff
	        binding_mode       #2f343a #900000 #ffffff
	}
```

#### Window placement ####

```
for_window [class="Chromium"] move to workspace 2

for_window [class="jetbrains-idea"] move to workspace 3
for_window [class="jetbrains-idea"] floating enable
for_window [class="jetbrains-idea" title="IntelliJ IDEA"] floating disable

for_window [class="Spotify"] move to workspace 10

for_window [title="Remmina Remote Desktop Client"] floating enable
```

### py3status config ###

In the `general` section, set main color to the ligher version chosen above for i3.

```
general {
        colors = true
        interval = 5
        color_good = "#06d4ac"
}
```

#### Preferred widgets ####

Compare with current defaults, update accordingly.

```
order += "disk /"
order += "wireless _first_"
order += "ethernet _first_"
order += "battery all"
order += "cpu_usage"
order += "memory"
order += "keyboard_layout"
order += "backlight"
order += "volume_status"
order += "tztime local"
```

#### Battery ####

Add consumption.

```
battery all {
        format = "%status %percentage %remaining (%consumption)"
}
```

#### Disk ####

Add label.

```
disk "/" {
        format = "Disk: %avail"
}
```

#### Memory ####

Use percentage_used and add label.

```
memory {
        format = "Mem: %percentage_used"
        threshold_degraded = "1G"
        format_degraded = "MEMORY < %available"
}
```

#### Date and time ####

Use more readable format.

```
tztime local {
        format = "%a %d. %m. %Y %H:%M:%S"
}
```

#### CPU usage ####

Add label.

```
cpu_usage {
        format = "Cpu: %usage"
}
```

#### Volume ####

Add icon. Colors that better show when over 100%.

```
volume_status {
        format = "[\?if=is_input 😮|♪]: {icon} {percentage}%"
        format_muted = "[\?if=is_input 😶|♪]: muted"
        thresholds =  [(0, 'good'), (101, 'bad')]
}
```

#### Keyboard ####

Label, colors.

```
keyboard_layout {
        format = "Kbd: {layout}"
        color_us = "#FFFFFF"
        color_cz = "#FF0000"
}
```

#### Backlight ####

Proper device.

```
backlight {
        device = "acpi_video0"
}
```

### Dunst config ###

Replace background color of urgency_normal with main color of chosen theme.

### Theme ###

Good one is `flat-remix`, from AUR, gtk+ as well as icons.

Use `lxappearance` to change current theme.

### Sound ###

Copy `/etc/pulse/daemon.conf` to `.config/pulse/daemon.conf` and uncomment and change the following lines:

```
resample-method = src-sinc-best-quality
enable-deferred-volume = no
default-sample-format = s32le
```

The first should improve overall sound quality. The second should solve issue with delayed Spotify volume change. The third allows for higher sample rate formats.

### External monitors ###

With nouveau installed, all outputs should be available without any configuration. All rendering is done on iGPU.

### Wifi after suspend ###

As desribed here: https://wiki.archlinux.org/index.php/MSI_GS65#Wifi_is_hardblocked_(airplane_mode)_after_waking_up_from_suspend

Wifi is hardblocked after wakeup. Need to enable airplane mode key combo as desribed here: https://wiki.archlinux.org/index.php/MSI_GS65#Airplane_Mode_Key_Combination

Pressing the combo twice after wakeup will reenable wifi.


### Display outputs don't work after suspend ###

Never suspend with monitors connected, wifi warns against that.

### Keyboard lighting ###

Install `msi-perkeyrgb`.

```
msi-perkeyrgb -m gs65 -s 00ff00
```

